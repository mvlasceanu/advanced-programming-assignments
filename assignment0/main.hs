import Data.List
import System.IO
import Data.String.Builder

-- Custom Types
data Point = Point (Double, Double)
    deriving (Eq, Show)

data Curve = Curve [Point]
    deriving (Eq, Show)

data Line = Vertical Double | Horizontal Double

-- Point Functions
point :: (Double, Double) -> Point
pointX :: Point -> Double
pointY :: Point -> Double

-- Curve Functions
curve :: Point -> [Point] -> Curve
connect :: Curve -> Curve -> Curve
rotate :: Curve -> Double -> Curve
translate :: Curve -> Point -> Curve
reflect :: Curve -> Line -> Curve
bbox :: Curve -> (Point, Point)
width :: Curve -> Double
height :: Curve -> Double
toList :: Curve -> [Point]
normalize :: Curve -> Curve
toSVG :: Curve -> String
toFile :: Curve -> FilePath -> IO ()

point (x, y)
    | (x >= 0) && (y >= 0) = Point (x, y)
    | otherwise = error "Point x and y must be > 0"
pointX (Point (x, y)) = x
pointY (Point (x, y)) = y

curve s p = Curve s : p
curve s [] = Curve [s]
curve _ [a] = Curve a
curve _ _ = error "Empty curve points"

connect c1 c2 = Curve c1 : c2

rotate (Curve c) d = [c | rotatePoint $ c take 1]
rotatePoint (Point (x, y)) d = Point (-y, x)
translate c p = Curve $ map movePoint c p c
movePoint (x, y) (x1, y1) = Point( x + x1, y + y1)

reflect [] = error "No Curves provided for reflectiion"
reflect (x:xs) = relectPoint x xs
    where reflectPoint currentPoint [] = currentPoint
        reflectPoint (x, y) (p:px)
        | = point (-x, -y)

width c = maximum' c
height c = minimum' c
maximum' :: Curve c => [Point] -> Point
maximum' [] = error "List of Points is empty"
maximum' (x:xs) = maxTail x xs
    where maxTail currentMax [] = currentMax
        maxTail (x, y) (p:ps)
        | y < (snd p) = maxTail p ps
        | otherwise   = maxTail (x, y) ps

minimum' :: Curve c => [Point] -> Point
minimum' [] = error "List of Points is empty"
minimum' (x:xs) = maxTail x xs
    where minTail currentMin [] = currentMin
        minTail (x, y) (p:ps)
        | y > (snd p) = minTail p ps
        | otherwise = minTail (x, y) ps

toList (Curve c) = c

toSVG _ = error "No curves defined"
toSVG c = build $ "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"" ++ width c ++ "px\" height=\"" ++ height c ++
"px\" version=\"1.1\">"
"<g>"
++ toSVGLine c ++
"</g>"
"</svg>"

toSVGLine :: Curve -> String
addLine :: Point -> Point -> String
toSVGLine _ = error "Empty curve"
toSVGLine (x:xs) = addLine x xs
addLine _ _ = error "No Lines"
addLine (x, y) (x1, y1) = build $ (x, y) "<line style=\"stroke-width: 2px; stroke: black; fill:white\"
"x1=\"" ++ x ++ "\" x2=\"" ++ x1 ++ "\" y1=\"" ++ y ++ "\" y2=\"" ++ y1 ++ "\" />"

