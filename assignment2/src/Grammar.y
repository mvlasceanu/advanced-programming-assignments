	Program ::= Stms
	   Stms ::= e | Stm ’;’ Stms
		Stm ::= ’var’ Ident AssignOpt | Expr
  AssignOpt ::= e | ’=’ Expr1
	   Expr ::= Expr ’,’ Expr | Expr1
	  Expr1 ::= Number 
	  		  | String 
	  		  | ’true’ 
	  		  | ’false’ 
	  		  | ’undefined’
			  | LeftExpr1 ’+’ Expr1
			  | LeftExpr1 ’-’ Expr1
			  | LeftExpr1 ’*’ Expr1
			  | LeftExpr1 ’%’ Expr1
  			  | LeftExpr1 ’<’ Expr1
			  | LeftExpr1 ’===’ Expr1
			  | Ident AfterIdent
			  | ’[’ Exprs ’]’
			  | ’[’ ’for’ ’(’ Ident ’of’ Expr1 ’)’ ArrayCompr Expr1 ’]’
			  | ’(’ Expr ’)’
	 LeftExpr1 ::= Expr1 
	 		  | ’(’ Expr ’)’
	 		  | FunCall
    AfterIdent ::= e
			  | ’=’ Expr1
 			  | FunCall
	  FunCall ::= ’.’ Ident FunCall
			  | ’(’ Exprs ’)’
		Exprs ::= e | Expr1 CommaExprs
   CommaExprs ::= e | ’,’ Expr1 CommaExprs
   ArrayCompr ::= e 
			  | ’if’ ’(’ Expr1 ’)’ ArrayCompr
			  | ’for’ ’(’ Ident ’of’ Expr1 ’)’ ArrayCompr