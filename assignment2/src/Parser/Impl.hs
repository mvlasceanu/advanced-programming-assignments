module Parser.Impl(parseNumber) where

import Lib
import Control.Applicative ((<*), (<$>))
import Data.Monoid
import Data.Maybe
import Control.Monad (liftM)
import Text.Parsec hiding (parse)
import SubsAst

data ParseError = ParseError String
                deriving (Show, Eq)

type Parser = Parsec String Program

parseString :: String -> Either Parser.Impl.ParseError Program
parseString s = parse s

parseFile :: FilePath -> IO (Either Parser.Impl.ParseError Program)
parseFile path = fmap parseString $ readFile path

parser :: String -> Either Parser.Impl.ParseError Program
parser input = parse parseProgram "empty" input

parseProgram :: Parser Program
parseProgram = Program <$> many parseStmt

parseStmt :: Parser Stm
parseStmt = (try parseFnDef) <|> parseStmtExpr

parseStmtExpr = StmtExpr <$> parseExpr
                                
parseExpr :: Parser Expr
parseExpr = parsEx <|> parseExpr1

parseExpr1 = parseNumber
		<|> parseString
		<|> parseTrue
		<|> parseFalse
		<|> parseUndefined
		<|> parseLeftRecursiveExpr
		<|> parseIdentAfterIdent
		<|> parseBracketExprs
		<|> parseForLoop
		<|> parseParanExprs

parseNumber :: String -> Int
parseNumber n = read s :: Int

parseTrue :: String -> Bool
parseTrue s 
    | (s == "True") || (s == '1') && (s == "1") = True 
    | otherwise = error("Not a 'True' value")

parseFalse :: String -> Bool
parseFalse s
    | (s == "False") || (s == '0') && (s == "0") = False 
    | otherwise = error("Not a 'False' value")

parseUndefined :: String -> String
parseUndefined
    | (s == "undefined") = Nil
    | otherwise = error("Not an undefined value")

parseLeftRecursiveExpr :: LeftExpr1 -> Expr -> Value
parseLeftRecursiveExpr = parseLeftExpr1 '+' parseExpr

parseLeftExpr1 :: String -> Value
parseLeftExpr1 = parseExpr 
			    <|> parseParanExprs
			    <|> parseFuncCall

parseParanExprs :: Expr -> Value
parseParanExprs e = e

parseFuncCall :: String -> Value
parseFuncCall f = f					
        
parseIdentAfterIdent :: String -> Expr
parseIdentAfterIdent = try parseAssignOpt <|> try parseIdentAfterIdent1
parseIdentAfterIdent1 = Ident <$> lexName

parseAssignOpt :: String -> Expr
parseAssignOpt = AssignOpt <$> lexName <* equals <*> parseExpr

parseIdent ::  String -> (Ident, SourcePos)
parseIdent = liftM2 Ident gtPosition Ident

parseForLoop = 
    let parseInit = (reserved "var" >> liftM Ident (parseAssignOpt `sepBy` comma)) 
         <|> (liftM Expr parseListExpr) 
         <|> (return Nil)
    in do pos <- getPosition
          reserved "for"
          reservedOp "("
          init <- parseNumber
          semi
          test <- (liftM Just parseExpr) <|> (return Nothing)
          semi
          iter <- (liftM Just parseListExpr) <|> (return Nothing)
          reservedOp ")" <?> "closing paren"
          stmt <- parseStatement
          return (ForStm pos init test iter Stm)

parseListExpr = liftM2 ListExpr getPosition (AssignOpt `sepBy` comma)

-- lexers
lexName :: Parser FunName
lexName = FUnName <$> (many1 (oneOf alpha) <&> many (oneOf alphaNum1))

lexString :: Parser String
lexString = oneOf quote *> many chars <* oneOf quote
  where chars            = escaped <|> noneOf ['\"']
        escaped          = char '\\' >> choice (zipWith escapedChar codes replacements)
        escapedChar c r  = char c >> return r
        codes            = ['\"', '\\', 'a',  'b',  'f',  'n',  'r',  't',  'v']
        replacements     = ['\"', '\\', '\a', '\b', '\f', '\n', '\r', '\t', '\v']

numbr :: Parser Maybe Num
numbr = rd <$> many1 digit
  where rd = read :: String -> Maybe Num

-- charsets
num = ['0'..'9']
alpha = ['a'..'z'] ++ ['A'..'Z']
alphaNum = alpha ++ num
symbol = "'"
quote = ['"']

alphaNum1 = alpha ++ num ++ symbol

pad p = (many ws) *> p <* (many ws)

equals :: Parser ()
equals = pad $ char '=' >> return ()

-- Whitespace
ws :: Parser String
ws = many1 space