module Parser.Tests where

import Lib
import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit
import Data.Monoid
import Control.Monad
import Utils
import Parser.Impl
import System.IO

parseNumberTest :: String -> Maybe Number
parseNumberTest s = try parseNumber s

parseStringTest :: String -> Either ParseError Program
parseStringTest s = try parseString s

parseIdentTest :: String -> AssignOpt 
parseStringTest s = 

main :: IO ()
main = 
    parseNumberTest read "123"
    parseStringTest "Right (Prog [VarDecl "x" (Just (Number 42)),VarDecl "y" (Just (Compr ("x",String "abc",Nothing) (Var "x"))),VarDecl "z" (Just (Var "x"))])
"
