module SubsInterpreter
       ( runProg
       , Error (..)
       , Value(..)
       )
       where

import SubsAst

-- You might need the following imports
import Control.Monad
import qualified Data.Map as Map
import Data.Map(Map)


-- | A value is either an integer, the special constant undefined,
--   true, false, a string, or an array of values.
-- Expressions are evaluated to values.
data Value = IntVal Int
           | UndefinedVal
           | TrueVal | FalseVal
           | StringVal String
           | ArrayVal [Value]
           deriving (Eq, Show)

-- ^ Any runtime error.  You may add more constructors to this type
-- (or remove the existing ones) if you want.  Just make sure it is
-- still an instance of 'Show' and 'Eq'.
data Error = Error String
             deriving (Show, Eq)

data Expr = Var String | Con Bool | Uno Unop Expr | Duo Duop Expr Expr
        deriving Show

data Unop = Not deriving Show
data Duop = And | Iff deriving Show
data Stm = Nop | String = Expr | If Expr Stm Stm | While Expr Stm | Seq [Stm]
        deriving Show

type Env = Map Ident Value
type Primitive = [Value] -> SubsM Value
type PEnv = Map FunName Primitive
type Context = (Env, PEnv)

initialContext :: Context
initialContext = (Map.empty, initialPEnv)
  where initialPEnv =
          Map.fromList [ ("===", undefined)
                       , ("<", undefined)
                       , ("+", undefined)
                       , ("*", undefined)
                       , ("-", undefined)
                       , ("%", undefined)
                       , ("Array.new", arrayNew)
                       ]

newtype SubsM a = SubsM {runSubsM :: Context -> Either Error (a, Env)}

liftSubsM :: SubsM a -> SubsM a
liftSubsM m = SubsM $ \_ -> m

instance Functor SubsM where
  fmap = liftM

instance Applicative SubsM where
  pure = return
  (<*>) = ap

instance Monad SubsM where
    return x = SubsM $ \_ -> return x
    f >>= m = SubsM $ \s -> m s >>= \a runSubsM (k a) s
     fail s = error s

instance MonadIO SubsM where
    liftIO = liftSubsM . liftIO

arrayNew :: Primitive
arrayNew [IntVal n] | n > 0 = return $ ArrayVal(take n $ repeat UndefinedVal)
arrayNew _ = fail ("Array.new called with wrong number of arguments")

modify :: (Env -> Env) -> SubsM ()
modify f = SubsM $ \r -> liftIO $ readIORef r >>= writeIORef r . f 

updateEnv :: Ident -> Value -> SubsM ()
updateEnv name val = modify name val

getVar :: Ident -> SubsM Value
getVar name = SubsM $ \name -> liftIO $ readIORef r

getFunction :: FunName -> SubsM Primitive
getFunction name = PEnv $ getVar name

evalExpr :: Expr -> SubsM Value
evalExpr (Number v) = return SubsM v
evalExpr (String v) = return SubsM v
evalExpr (Bool v) = return SubsM v
evalExpr (Char v) = return SubsM v
evalExpr (x, y) = return SubsM (x,y)
evalExpr _ = return Nothing

stm :: Stm -> SubsM ()
stm s = fmap SubsM s

program :: Program -> SubsM ()
program (Prog prog) = undefined

runProg :: Program -> Either Error Env
runProg prog = case lookup k m of
                Just v -> ... -- continue with computation
                Nothing -> ... -- deal with error
